package Models

import (
	"fmt"
	"latihan-gin/Config"
)

func GetAllUsers(user *[]User) (err error) {
	if err = Config.DB.Find(user).Error; err != nil{
		return err
	}
	return nil
}

func GetUserByID(user *User, uid string) (err error) {
	if err = Config.DB.Where("id = ?",uid).First(user).Error; err != nil {
		return err
	}
	return nil
}

func CreateUser(user *User) (err error) {
	if err = Config.DB.Create(user).Error; err != nil{
		return err
	}
	return nil
}

func UpdateUser(user *User, uid string) (err error) {
	fmt.Println(user)
	Config.DB.Save(user)
	return nil
}

func DeleteUser(user *User, uid string) (err error) {
	Config.DB.Where("id = ?",uid).Delete(user)
	return nil
}