package main

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"latihan-gin/Config"
	"latihan-gin/Models"
	"latihan-gin/Routes"
)

var err error

func main() {
 Config.DB, err = gorm.Open("mysql", Config.DBUrl(Config.BuildDBConfig()))
	if err != nil {
		fmt.Println("Status : ",err)
	}

	defer Config.DB.Close()
 Config.DB.AutoMigrate(&Models.User{})

 r := Routes.SetUpRouter()
 r.Run()
}