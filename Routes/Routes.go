package Routes

import (
	"github.com/gin-gonic/gin"
	"latihan-gin/Controllers"
)

func SetUpRouter() *gin.Engine {
	r := gin.Default()
	grp1 := r.Group("/user-api")
	{
		grp1.GET("users", Controllers.GetUsers)
		grp1.GET("user/:id", Controllers.GetUserByID)
		grp1.POST("user",Controllers.CreateUser)
		grp1.PUT("user/:id", Controllers.UpdateUser)
		grp1.DELETE("user/:id",Controllers.DeleteUser)
	}
	return r
}